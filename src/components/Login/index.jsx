import React from 'react';
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { TextField, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useState } from "react";
import axios from "axios";

const useStyles = makeStyles((theme) => ({
    root: {
        margin: theme.spacing(1),
        display: 'flex',
        justifyContent: 'center',
    },

    form: {
        display: 'flex',
        flexDirection: 'column',
        position: 'absolute',
        top: '30%',
        left: '50%',
        transform: 'translateX(-50%)',
        border: '1px solid #c4c4c4',
        padding: '100px 10px',
        borderRadius: '5px',
        backgroundColor: '#fff',
        textAlign: 'center',
    },
    TextField: {
        margin: '10px'
    },
    Button: {
        margin: '10px'
    },
    MessageError: {
        color: 'red',
        height: '19px'
    },
    MessageLogin: {
        color: 'green',
        height: '19px'
    }
}))


const Login = () => {

    const classes = useStyles();

    const [erro, setErro] = useState()
    const [isLogged, setIsLogged] = useState()

    const formSchema = yup.object().shape({
        username: yup.string().required("Nome Obrigatório"),
        password: yup.string().required("Senha obrigatória")
    })

    const { register, handleSubmit, formState: { errors } } = useForm({
        resolver: yupResolver(formSchema)
    })

    const onSubmitFunction = (data) => {
        axios.post("https://kenzieshop.herokuapp.com/sessions/", data)
            .then((response) => {
                setIsLogged(response)
                setErro()
            })
            .catch((err) => {
                setErro(err)
                setIsLogged()
            })
    }

    return (
        <div className={classes.root} >
            <form className={classes.form} onSubmit={handleSubmit(onSubmitFunction)}>
                <TextField className={classes.TextField} label="user name" variant="outlined" {...register("username")} required />
                <TextField className={classes.TextField} type="password" label="Senha" variant="outlined" {...register("password")} required />
                <Button className={classes.Button} type="submit" variant="contained" color="primary">Entrar</Button>
                {!!erro && <p className={classes.MessageError}>Requisição Falhou!</p>}
                {!!isLogged && <p className={classes.MessageLogin}>Requisição Aceita!</p>}
            </form>
        </div>
    )
}

export default Login